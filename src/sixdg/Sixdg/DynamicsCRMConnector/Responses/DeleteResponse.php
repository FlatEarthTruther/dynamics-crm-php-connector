<?php

namespace Sixdg\DynamicsCRMConnector\Responses;

/**
 * Class DeleteResponse
 *
 * @package Sixdg\DynamicsCRMConnector\Responses
 */
class DeleteResponse extends \DOMDocument
{
    /**
     * Returns true if the entity has been deleted
     *
     * @returns boolean
     */
    public function success()
    {
        //If DeleteResponse tag found return true
        foreach ($this->getElementsByTagName('DeleteResponse') as $node) {
            return true;
        }

        return false;
    }
}
