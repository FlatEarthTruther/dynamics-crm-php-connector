<?php
namespace Sixdg\DynamicsCRMConnector\Controllers;

use Sixdg\DynamicsCRMConnector\Builders\RequestBuilder;
use Sixdg\DynamicsCRMConnector\DynamicsCRMConnector;
use Sixdg\DynamicsCRMConnector\Models\Entity;
use Sixdg\DynamicsCRMConnector\Queries\FetchXML;
use Sixdg\DynamicsCRMConnector\Responses\CreateEntityResponse;
use Sixdg\DynamicsCRMConnector\Responses\RetrieveMultipleResponse;
use Sixdg\DynamicsCRMConnector\Responses\RetrieveResponse;
use Sixdg\DynamicsCRMConnector\Responses\DeleteResponse;

/**
 * Class DynamicsCRMController
 *
 * @package Sixdg\DynamicsCRMConnector\Controllers
 */
class DynamicsCRMController
{
    private $cache;
    private $config;
    private $connector;
    private $securityToken;
    private $lastInsertId;

    /**
     * @param DynamicsCRMConnector $connector
     * @param array                $config
     * @param object               $cache
     */
    public function __construct(
        DynamicsCRMConnector $connector,
        array $config,
        $cache = null
    ) {
        $this->connector = $connector;
        $this->config = $config;
        $this->cache = $cache;
    }

    /**
     * Finds an entity by id
     *
     * @param string $entityName
     * @param string $entityId
     *
     * @return Entity
     */
    public function find($entityName, $entityId)
    {
        $request = $this->getNewRequestBuilder()->getRequest('RetrieveRequest');
        $request->setEntityName($entityName);
        $request->setEntityId($entityId);

        //send the soap request
        $requester = $this->connector->getRequester();
        $requester->setResponder(new RetrieveResponse());
        $response = $requester->sendRequest($request->getTo(), $request->getXML());

        //return the hydrated entity
        $entity = $response->getEntity($this->connector->getEntityFactory());

        return $entity;
    }

    /**
     * Find all the instances of the entity given
     * Check the FetchXML schema for filters operators information (http://msdn.microsoft.com/en-us/library/bb930489.aspx)
     *
     * @param FetchXML $query
     * @param boolean  $hydrate    whether to return an array or a Entity
     * @param int      $pageNumber The specific page to be retrieved
     * @param int      $limit      The number of items per page
     *
     * @return array
     */
    public function findAll(FetchXML $query, $hydrate = true, $pageNumber = null, $limit = null)
    {
        $request = $this->getNewRequestBuilder()
            ->getRequest('RetrieveMultipleRequest');
        $request->setQuery($query);

        if ($limit) {
            $request->setLimit($limit);
        }
        if ($pageNumber) {
            $request->setPageNumber($pageNumber);
        }

        $entities = array();
        do {
            $response = $this->sendRetrieveMultipleRequest($request, $pageNumber, $hydrate, $entities);
            //if the pageNumber's been set by the user we exit the loop and return that page only, otherwise loop until no more records
        } while ($response->getMoreRecords() === 'true' and !$pageNumber);

        return $entities;
    }

    private function sendRetrieveMultipleRequest($request, $pageNumber, $hydrate, &$entities)
    {
        //send the request to the server
        $requester = $this->connector->getRequester();
        $requester->setResponder(new RetrieveMultipleResponse());
        $response = $requester->sendRequest($request->getTo(), $request->getXML());
        //create and load the response
        if ($hydrate) {
            $entities = array_merge($entities, $response->getEntities($this->connector->getEntityFactory()));
        } else {
            $entities = array_merge($entities, $response->asArray());
        }
        if (!$pageNumber) {
            //update the request to retrieve the next page
            $request->setPagingCookie($response->getPagingCookie());
            $request->setPageNumber($response->getPageNumber() + 1);
        }

        return $response;
    }

    /**
     * @param Entity $entity
     *
     * @return mixed
     */
    public function create($entity)
    {
        $this->connector->getEntityValidator()->validate("create", $entity);

        $request = $this->getNewRequestBuilder()
            ->setEntity($entity)
            ->getRequest('CreateEntityRequest');

        $requester = $this->connector->getRequester();
        $requester->setResponder(new CreateEntityResponse());
        $response = $requester->sendRequest($request->getTo(), $request->getXML());

        $arrayResponse = $response->asArray();
        $this->lastInsertId = $arrayResponse['CreateResult'];

        return $response->isSuccess();
    }

    public function getLastInsertId()
    {
        if ($this->lastInsertId) {
            return $this->lastInsertId;
        }

        return 0;
    }

    /**
     * @param Entity $entity
     *
     * @return mixed
     */
    public function update($entity)
    {
        $this->connector->getEntityValidator()->validate("update", $entity);

        $request = $this->getNewRequestBuilder()
            ->setEntity($entity)
            ->getRequest('UpdateEntityRequest');

        $response = $this->connector->getRequester()->sendRequest($request->getTo(), $request->getXML());

        return isset($response);
    }

    /**
     * @param Entity $entity
     *
     * @return mixed
     */
    public function findMetadata($entity)
    {
        $request = $this->getNewRequestBuilder()
            ->setEntity($entity)
            ->getRequest('RetrieveEntityRequest');

        return $this->connector->getRequester()->sendRequest($request->getTo(), $request->getXML());
    }

    /**
     * @return RequestBuilder
     */
    private function getNewRequestBuilder()
    {
        $securityToken = $this->getSecurityToken();

        $requestBuilder = $this->connector->getRequestBuilder()->reset();

        return $requestBuilder->setServer($this->config['crm'])
            ->setCrm($this->config['crm'])
            ->setOrganization($this->config['organization'])
            ->setSecurityToken($securityToken);
    }

    /**
     * @return array|bool
     */
    private function getSecurityToken()
    {
        if (!$this->securityToken) {
            $this->securityToken = $this->connector->getSecurityService()->login(
                $this->config['adfs'],
                $this->config['crm'],
                $this->config['crm'] . "/XRMServices/2011/Discovery.svc",
                $this->config['username'],
                $this->config['password']
            );
        }

        return $this->securityToken;
    }

    /**
     * Returns the cache object
     *
     * @return CacheInterface
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * Deletes an entity by id
     *
     * @param string $entityName
     * @param string $entityId
     *
     * @return Entity
     */
    public function delete($entityName, $entityId)
    {
        $request = $this->getNewRequestBuilder()->getRequest('DeleteRequest');
        $request->setEntityName($entityName);
        $request->setEntityId($entityId);

        //send the soap request
        $requester = $this->connector->getRequester();
        $requester->setResponder(new DeleteResponse());
        $response = $requester->sendRequest($request->getTo(), $request->getXML());

        return $response->success();
    }
}
