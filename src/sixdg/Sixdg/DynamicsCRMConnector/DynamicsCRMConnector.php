<?php
namespace Sixdg\DynamicsCRMConnector;

use Go\Core\AspectKernel;
use Psr\Log\LoggerInterface;
use Sixdg\DynamicsCRMConnector\Aspects\ControllerAspect;
use Sixdg\DynamicsCRMConnector\Builders\RequestBuilder;
use Sixdg\DynamicsCRMConnector\Components\DOM\DOMHelper;
use Sixdg\DynamicsCRMConnector\Components\Soap\SoapRequester;
use Sixdg\DynamicsCRMConnector\Components\Time\TimeHelper;
use Sixdg\DynamicsCRMConnector\Controllers\DynamicsCRMController;
use Sixdg\DynamicsCRMConnector\Factories\EntityFactory;
use Sixdg\DynamicsCRMConnector\Factories\MetadataRegistryFactory;
use Sixdg\DynamicsCRMConnector\Services\EntityToDomConverter;
use Sixdg\DynamicsCRMConnector\Services\EntityValidator;
use Sixdg\DynamicsCRMConnector\Services\SecurityService;

/**
 * Class DynamicsCRMConnector
 */
class DynamicsCRMConnector
{
    private $aspectKernel;
    private $logger;
    private $controller;
    private $securityService;
    private $requestBuilder;
    private $entityToDomConverter;
    private $domHelper;

    public function __construct($config, AspectKernel $aspectKernel, LoggerInterface $logger, $cache = null)
    {
        $this->aspectKernel = $aspectKernel;
        $this->logger = $logger;
        $this->registerAspects();

        $this->domHelper = new DOMHelper();
        $this->entityToDomConverter = new EntityToDomConverter($this->domHelper);
        $this->requestBuilder = new RequestBuilder(new DOMHelper(), new TimeHelper(), $this->entityToDomConverter);
        $this->securityService = new  SecurityService($this->requestBuilder, new SoapRequester());

        $this->controller = new DynamicsCRMController($this, $config, $cache);

        return $this;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getEntityFactory()
    {
        return new EntityFactory($this->controller, new MetadataRegistryFactory($this->controller));
    }

    public function getSecurityService()
    {
        return $this->securityService;
    }

    public function getRequestBuilder()
    {
        return $this->requestBuilder;
    }

    public function getRequester()
    {
        return new SoapRequester();
    }

    private function registerAspects()
    {
        $this->aspectKernel->getContainer()->registerAspect(new ControllerAspect($this->logger));
    }

    public function getEntityValidator()
    {
        return new EntityValidator();
    }
}
