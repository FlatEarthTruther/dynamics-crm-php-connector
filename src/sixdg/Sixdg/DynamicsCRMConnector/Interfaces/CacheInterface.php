<?php
namespace Sixdg\DynamicsCRMConnector\Interfaces;

/**
 * Defines an interface to be implemented by any caching object
 *
 * @author leandro.miranda
 */
interface CacheInterface
{
    /**
     * Defines a prefix to be used by the library to avoid key conflicts
     *
     * @return string
     */
    public function getKeyPrefix();

    /**
     * Return an object from the cache engine
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key);

    /**
     * Checks if an object exists in the cache engine
     * @param  string $key
     * @return bool
     */
    public function exists($key);

    /**
     * Stores an object in the cache engine
     *
     * @param  string $key
     * @param  mixed  $value
     * @param  int    $expiration Expiration time in seconds
     * @return bool
     */
    public function set($key, $value, $expiration = 0);
}
