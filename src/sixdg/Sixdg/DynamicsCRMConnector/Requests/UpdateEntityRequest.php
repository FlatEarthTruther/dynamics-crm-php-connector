<?php
namespace Sixdg\DynamicsCRMConnector\Requests;

/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 12/07/13
 * Time: 10:12
 */
use Sixdg\DynamicsCRMConnector\Requests\CreateEntityRequest;

/**
 * Class UpdateEntityRequest
 */
class UpdateEntityRequest extends CreateEntityRequest
{
    protected $action = 'http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Update';
    protected $to = 'XRMServices/2011/Organization.svc';

    /**
     * @param \DOMElement $body
     *
     * @return mixed
     */
    protected function getBodyContents($body)
    {
        $updateNode = $this->domHelper->createElementNS($this->serviceNS, 'Update');
        $this->setSchemaInstance($updateNode);
        $updateNode->appendChild($this->getEntityNode($body));

        return $updateNode;
    }
}
