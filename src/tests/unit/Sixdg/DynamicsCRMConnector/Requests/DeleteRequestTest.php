<?php

use Sixdg\DynamicsCRMConnector\Test\BaseTest;

/**
 * Class DeleteRequestTest
 */
class DeleteRequestTest extends BaseTest
{
    /**
     * @var RequestBuilder
     */
    protected $requestBuilder;

    protected $entityRequest;

    public function setUp()
    {
        $this->requestBuilder = $this->getAndBootstrapRequestBuilder();
    }

    public function testDeleteRequest()
    {
        $request = $this->requestBuilder->getRequest('DeleteRequest');

        $request->setEntityName('account');
        $request->setEntityId('832ed501-8d10-e311-a82b-005056881769');

        $xml = $request->getXML();
        $doc = new \DOMDocument;
        $doc->loadXML($xml);

        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;

        $expected = new \DOMDocument;
        $expected->loadXML(file_get_contents(__DIR__ . '/Fixtures/DeleteRequest.xml'));
        $expected->preserveWhiteSpace = false;
        $expected->formatOutput = true;

        $this->assertEquals($expected->saveXML(), $doc->saveXML());
    }
}
