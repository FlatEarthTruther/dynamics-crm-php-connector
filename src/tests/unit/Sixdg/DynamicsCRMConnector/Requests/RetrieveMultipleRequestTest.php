<?php

use Sixdg\DynamicsCRMConnector\Test\BaseTest;
use Sixdg\DynamicsCRMConnector\Queries\FetchXML;

/**
 * Class RetrieveMultipleRequestTest
 *
 * @author Leandro Miranda
 */
class RetrieveMultipleRequestTest extends BaseTest
{

    protected $request;

    public function setUp()
    {
        $requestBuilder = $this->getAndBootstrapRequestBuilder();
        $this->request = $requestBuilder->getRequest('RetrieveMultipleRequest');

                //build the query
        $query = new FetchXML();
        $query->setEntityName('contact');
        $query->addAnd([
            'attribute' => 'lastname',
            'operator' => 'not-null',
        ]);

        $this->request->setQuery($query);
    }

    /**
     * Test that the RequestMultiple tag matches the expected
     */
    public function testGetXml()
    {
        $requestXML = $this->request->getXML();
        $doc = new \DOMDocument;
        $doc->loadXML($requestXML);
        //we only check for the RetrieveMultiple tag
        $retrieveMultipleRequest = $doc->getElementsByTagName('RetrieveMultiple');
        if ($retrieveMultipleRequest && 0 < $retrieveMultipleRequest->length) {
            $retrieveMultipleRequest = $retrieveMultipleRequest->item(0);
            $retrieveMultipleRequestXML = $doc->saveXML($retrieveMultipleRequest);
        }

        $expected = file_get_contents(__DIR__ . '/Fixtures/RetrieveMultipleRequest.xml');

        $this->assertEquals($expected, $retrieveMultipleRequestXML);
    }
}
