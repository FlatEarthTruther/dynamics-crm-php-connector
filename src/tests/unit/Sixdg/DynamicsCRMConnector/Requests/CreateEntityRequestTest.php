<?php
use Sixdg\DynamicsCRMConnector\Builders\RequestBuilder;
use Sixdg\DynamicsCRMConnector\Test\BaseTest;

/**
 * Class CreateEntityRequestTest
 */
class CreateEntityRequestTest extends BaseTest
{
    protected $requestBuilder;
    protected $domHelper;
    protected $entityToDomConverter;

    public function setUp()
    {
        $domHelper = new Sixdg\DynamicsCRMConnector\Components\DOM\DOMHelper();

        $timeHelper = \Mockery::mock('Sixdg\DynamicsCRMConnector\Components\Time\TimeHelper');
        $timeHelper->shouldReceive('getCurrentTime')->andReturn('2013-07-04T10:34:51.00Z');
        $timeHelper->shouldReceive('getExpiryTime')->andReturn('2013-07-04T10:35:51.00Z');

        $this->entityToDomConverter = new Sixdg\DynamicsCRMConnector\Services\EntityToDomConverter($domHelper);
        $this->requestBuilder = new RequestBuilder($domHelper, $timeHelper, $this->entityToDomConverter);
        $this->requestBuilder->setSecurityToken($this->getSecurityToken())
            ->setOrganization($GLOBALS['config']['organization'])
            ->setServer($GLOBALS['config']['crm']);

    }

    private function getSecurityToken()
    {
        return [
            'securityToken' => '<MockSecurityTokenResponse></MockSecurityTokenResponse>',
            'binarySecret'  => '2Y/D9fcqP6YIE/NEMN6h0+u/mMShjqy6P6HC9C4cxPo=',
            'keyIdentifier' => ' _18529f8f-0e27-4dd8-ac82-b34cb54ae302',
        ];
    }

    public function testCreateContactEntityRequest()
    {
        $entityFactory = $this->getEntityFactory('contact');
        $entity = $entityFactory->makeEntity('contact');
        $entity->setFirstName('Test firstname');
        $entity->setLastName('Test lastname');

        $request = $this->requestBuilder
                        ->setEntity($entity)
                        ->getRequest('CreateEntityRequest');

        $xml = $request->getXML();

        $doc = new \DOMDocument;
        $doc->loadXML($xml);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;

        $expected = new \DOMDocument;
        $expected->loadXML(file_get_contents(__DIR__ . '/Fixtures/CreateContactEntityRequest.xml'));
        $this->assertExpected($expected, $doc);
    }

    public function testCreateAccountEntityRequest()
    {
        $entityFactory = $this->getEntityFactory('account');
        $entity = $entityFactory->makeEntity('account');
        $entity->setName('Test name');
        $entity->setCustomertypecode(100000001);
        $entity->setNew_ebillzbst(1);

        $request = $this->requestBuilder
                        ->setEntity($entity)
                        ->getRequest('CreateEntityRequest');

        $xml = $request->getXML();

        $doc = new \DOMDocument;
        $doc->loadXML($xml);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;

        $expected = new \DOMDocument;
        $expected->loadXML(file_get_contents(__DIR__ . '/Fixtures/CreateAccountEntityRequest.xml'));
        $this->assertExpected($expected, $doc);
    }
}
