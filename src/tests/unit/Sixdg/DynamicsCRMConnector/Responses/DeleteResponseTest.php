<?php

use Sixdg\DynamicsCRMConnector\Responses\DeleteResponse;
use Sixdg\DynamicsCRMConnector\Test\BaseTest;

/**
 * @author Leandro Miranda
 * @date 29/08/2013
 */
class DeleteResponseTest extends BaseTest
{

    /**
     * @var Sixdg\DynamicsCRMConnector\Components\Responses\DeleteResponse
     */
    protected $response;

    public function setUp()
    {
        $this->response = new DeleteResponse();
        $this->domHelper = new \DOMDocument();
    }

    public function testSuccess()
    {
        $this->domHelper->load(__DIR__ . '/Fixtures/deleteResponse.xml');
        $this->response->loadXML($this->domHelper->saveXML());
        $success = $this->response->success();
        $this->assertTrue($success);
     }

    public function testFail()
    {
        $this->domHelper->load(__DIR__ . '/Fixtures/retrieveResponse.xml');
        $this->response->loadXML($this->domHelper->saveXML());
        $success = $this->response->success();
        $this->assertFalse($success);
    }
}
