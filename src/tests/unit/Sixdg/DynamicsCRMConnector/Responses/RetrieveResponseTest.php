<?php

use Sixdg\DynamicsCRMConnector\Responses\RetrieveResponse;
use Sixdg\DynamicsCRMConnector\Test\BaseTest;

/**
 * @author Leandro Miranda
 * @date 18/07/2013
 */
class RetrieveResponseTest extends BaseTest
{

    /**
     * @var Sixdg\DynamicsCRMConnector\Components\Responses\DynamicsCRMResponse
     */
    protected $response;

    public function setUp()
    {
        $this->response = new RetrieveResponse();
        $this->response->load(__DIR__ . '/Fixtures/retrieveResponse.xml');
    }

    public function testGetFormattedValues()
    {
        $formattedValues = $this->response->asArray(true);
        $formattedValues = $formattedValues[0];
        $this->assertTrue($formattedValues['donotsendmm'] == 'Send');
        //check if contactid exist as this is a value that doesn't exist in the Formatted values
        $this->assertNotEmpty($formattedValues['contactid']);
    }

    public function testGetRawValues()
    {
        $rawValues = $this->response->asArray(false);
        $rawValues = $rawValues[0];
        $this->assertTrue($rawValues['donotsendmm'] == 'false');
        $this->assertNotEmpty($rawValues['contactid']);
    }

    public function testGetEntity()
    {
        $entityFactory = $this->getEntityFactory('contact');

        $contact = $this->response->getEntity($entityFactory);

        $this->assertEquals(get_class($contact), 'Sixdg\DynamicsCRMConnector\Models\Entity');
        $this->assertEquals('TEST UPDATE AGAIN', $contact->getlastname());
    }
}
