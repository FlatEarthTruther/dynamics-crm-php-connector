<?php
/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 04/07/13
 * Time: 09:59
 */
return [
    'username'     => '6dgdev\svc_j6crm',
    'password'     => 'Pa55w0rd',
    'crm'          => 'https://internalcrm.6dgdev.co.uk:444/',
    'adfs'         => 'https://sts1.6dgdev.co.uk/adfs/',
    'organization' => 'J6testing',
    'discoveryUrl' => 'XRMServices/2011/Discovery.svc'
];
